package pe.uni.armandollueng.mitrivia;

public class Pregunta {

    int nombre;
    int image;
    boolean esVerdadera;

    public Pregunta(int nombre, int image, boolean esVerdadera) {
        this.nombre = nombre;
        this.image = image;
        this.esVerdadera = esVerdadera;
    }
}
