package pe.uni.armandollueng.mitrivia;

import androidx.appcompat.app.AppCompatActivity;

import android.app.Application;
import android.os.Bundle;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.google.android.material.snackbar.Snackbar;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity {

    TextView textViewPregunta;
    Button buttonVerdadero;
    Button buttonFalso;
    Button buttonSiguiente;
    Button buttonAnterior;
    ImageView imageView;
    ArrayList<Pregunta> preguntas = new ArrayList<>();
    int posicionActual = 0;
    LinearLayout linearLayout;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        textViewPregunta = findViewById(R.id.text_view_pregunta);
        buttonAnterior = findViewById(R.id.anterior_button);
        buttonSiguiente = findViewById(R.id.siguiente_button);
        buttonVerdadero = findViewById(R.id.true_button);
        buttonFalso = findViewById(R.id.false_button);
        imageView = findViewById(R.id.image_view);
        linearLayout =  findViewById(R.id.linear);

        llenarPreguntas();

        buttonVerdadero.setOnClickListener(v -> {
            if(preguntas.get(posicionActual).esVerdadera){
                Snackbar.make(linearLayout, R.string.msg_snack_bar_respCorr, Snackbar.LENGTH_INDEFINITE).setAction("cerrar", v1 -> {
                }).show();
            }else{
                Snackbar.make(linearLayout, R.string.msg_snack_bar_respInc, Snackbar.LENGTH_INDEFINITE).setAction("cerrar", v1 -> {
                }).show();
            }
        });

        buttonFalso.setOnClickListener(v -> {
            if(!preguntas.get(posicionActual).esVerdadera){
                Snackbar.make(linearLayout, R.string.msg_snack_bar_respCorr, Snackbar.LENGTH_INDEFINITE).setAction("cerrar", v1 -> {
                }).show();
            }else{
                Snackbar.make(linearLayout, R.string.msg_snack_bar_respInc, Snackbar.LENGTH_INDEFINITE).setAction("cerrar", v1 -> {
                }).show();
            }

        });

        buttonSiguiente.setOnClickListener(v -> {
            if(posicionActual != preguntas.size()-1){
                posicionActual++;
            }else{
                posicionActual = 0;
            }

            textViewPregunta.setText(preguntas.get(posicionActual).nombre);
            imageView.setImageResource(preguntas.get(posicionActual).image);
        });

        buttonAnterior.setOnClickListener(v -> {
            if(posicionActual != 0){
                posicionActual--;
            }else{
                posicionActual = preguntas.size()-1;
            }

            textViewPregunta.setText(preguntas.get(posicionActual).nombre);
            imageView.setImageResource(preguntas.get(posicionActual).image);

        });
    }

    private void llenarPreguntas(){
        preguntas.add(new Pregunta(R.string.uno, R.drawable.uno, false));
        preguntas.add(new Pregunta(R.string.dos, R.drawable.dos, false));
        preguntas.add(new Pregunta(R.string.tres, R.drawable.tres, false));
        preguntas.add(new Pregunta(R.string.cuatro, R.drawable.cuatro, true));
        preguntas.add(new Pregunta(R.string.cinco, R.drawable.cinco, false));
        preguntas.add(new Pregunta(R.string.seis, R.drawable.seis, false));
        preguntas.add(new Pregunta(R.string.siete, R.drawable.siete, true));
    }
}

